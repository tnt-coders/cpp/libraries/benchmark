from conans import ConanFile, CMake, tools

class Benchmark(ConanFile):
    name = "benchmark"
    description = "A simple way to benchmark C++ routines"
    url = "https://gitlab.com/tnt-coders/cpp/libraries/benchmark.git"
    license = "GNU Lesser General Public License v3.0"
    author = "TNT Coders <tnt-coders@googlegroups.com>"

    topics = ("benchmark")

    settings = ("os", "compiler", "build_type", "arch")

    options = {
        "shared": [True, False],
    }

    default_options = {
        "shared": False,
    }

    build_requires = (
        "catch2/3.0.0@tnt-coders/stable",
    )

    exports_sources = ("cmake/*", "docs/*", "include/*", "src/*", "test/*", "CMakeLists.txt")

    generators = ("cmake", "cmake_paths")

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.configure()
        return cmake

    def configure(self):
        tools.check_min_cppstd(self, "17")

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()
        cmake.test()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)